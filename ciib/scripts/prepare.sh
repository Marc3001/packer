#!/bin/bash
set -e

echo "Prepare | Update apt"
apt-get update

echo "Prepare | Install quemu-guest-agent"
apt-get install -y qemu-guest-agent

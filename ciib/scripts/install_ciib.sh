#!/bin/bash
set -e

echo "Install ciib | Install dependancies"
apt-get update
apt-get install -y nginx python3-pip
apt-get clean

echo "Install ciib | Install python packages"
pip install ciib --index-url https://gitlab.com/api/v4/projects/56556135/packages/pypi/simple
pip install "uvicorn[standard]" gunicorn

echo "Install ciib | Create missing folders"
mkdir /var/iso
chown www-data.www-data /var/iso

echo "Install ciib | Setup services"
cat << EOF > /etc/systemd/system/gunicorn.service
[Service]
[Unit]
Description=gunicorn daemon
Requires=gunicorn.socket
After=network.target

[Service]
Type=notify
# the specific user that our service will run as
User=www-data
Group=www-data
# another option for an even more restricted service is
# DynamicUser=yes
# see http://0pointer.net/blog/dynamic-users-with-systemd.html
RuntimeDirectory=gunicorn
WorkingDirectory=/var/www
ExecStart=/usr/local/bin/gunicorn ciib:app --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind 0.0.0.0:80
ExecReload=/bin/kill -s HUP \$MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF

cat << EOF > /etc/systemd/system/gunicorn.socket
[Unit]
Description=gunicorn socket

[Socket]
ListenStream=/run/gunicorn.sock
# Our service won't need permissions for the socket, since it
# inherits the file descriptor by socket activation
# only the nginx daemon will need access to the socket
SocketUser=www-data
# Optionally restrict the socket permissions even more.
# SocketMode=600

[Install]
WantedBy=sockets.target
EOF

systemctl daemon-reload
systemctl enable gunicorn.socket

echo "Install ciib | Setup nginx"
rm -f /etc/nginx/sites-enabled/default

cat << EOF > /etc/nginx/sites-enabled/ciib
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www;
    index index.html index.htm index.nginx-debian.html;

    server_name _;

    location / {
        proxy_set_header   Host                             \$host;
        proxy_set_header   X-Real-IP                        \$remote_addr;
        proxy_set_header   X-Forwarded-For                  \$proxy_add_x_forwarded_for;
        proxy_pass         http://unix:/run/gunicorn.sock;
    }
}
EOF

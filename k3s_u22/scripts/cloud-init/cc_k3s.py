# This file is part of cloud-init. See LICENSE file for license information.
"""Example Module: Shows how to create a module"""

import logging
import subprocess
from cloudinit.cloud import Cloud
from cloudinit.config import Config
from cloudinit.config.schema import MetaSchema, get_meta_doc
from cloudinit.distros import ALL_DISTROS
from cloudinit.settings import PER_INSTANCE

MODULE_DESCRIPTION = """\
Description that will be used in module documentation.

This will likely take multiple lines.
"""

LOG = logging.getLogger(__name__)

meta: MetaSchema = {
    "id": "cc_k3s",
    "name": "k3s module",
    "title": "will initialize k3s node/cluster",
    "description": MODULE_DESCRIPTION,
    "distros": [ALL_DISTROS],
    "frequency": PER_INSTANCE,
    "activate_by_schema_keys": ["k3s"],
    "examples": [
        "example_key: example_value",
        "example_other_key: ['value', 2]",
    ],
}

__doc__ = get_meta_doc(meta)


def handle(
    name: str, cfg: Config, cloud: Cloud, args: list
) -> None:
    LOG.debug(f"BEGIN module {name}")

    # Init command
    init_env_vars = {
        "INSTALL_K3S_VERSION": cfg['k3s']['version'],
        "K3S_KUBECONFIG_MODE": "644",
        "INSTALL_K3S_EXEC": cfg['k3s']['mode'],
    }
    init_cmd = [
        "/usr/local/bin/install_k3s",
        "--token",
        cfg['k3s']['token'],
        "--server",
        cfg['k3s']['master_url'],
    ]
    for label in cfg['k3s']['labels']:
        init_cmd.append("--node-label")
        init_cmd.append(label)
    LOG.debug(f"init command: {' '.join(init_cmd)}")
    subprocess.call(init_cmd, env=init_env_vars, shell=False)

    # Post init command (kubectl annotate)
    # if len(cfg['k3s']['annotations']) > 0:
    #     post_init_cmd = [
    #         "/usr/local/bin/kubectl",
    #         "annotate",
    #         "--overwrite",
    #         f"node/{cfg['hostname']}",
    #     ]
    #     for annotation in cfg['k3s']['annotations']:
    #         post_init_cmd.append(annotation)
    #     LOG.debug(f"post init command: {' '.join(post_init_cmd)}")

    LOG.debug(f"END module {name}")

#!/usr/bin/env bash

echo "K3s | Install cloud-init module"
mv /home/ubuntu/cc_k3s.py /usr/lib/python3/dist-packages/cloudinit/config/cc_k3s.py
chown root.root /usr/lib/python3/dist-packages/cloudinit/config/cc_k3s.py

echo "K3s | Enabling cloud-init module"
mv /home/ubuntu/cloud.cfg /etc/cloud/cloud.cfg
chown root.root /etc/cloud/cloud.cfg

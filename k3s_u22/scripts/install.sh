#!/usr/bin/env bash

echo "Install | Update apt"
apt-get update

echo "Install | Install quemu-guest-agent"
apt-get install -y qemu-guest-agent

echo "Install | Install nfs-common for longhorn"
apt-get install -y nfs-common

echo "Install | Install k3s installer"
wget -O /usr/local/bin/install_k3s https://get.k3s.io
chmod +x /usr/local/bin/install_k3s

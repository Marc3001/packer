packer {
  required_plugins {
    qemu = {
      source  = "github.com/hashicorp/qemu"
      version = "~> 1"
    }
  }
}

locals {
  ubuntu_version = "jammy"
  vm_name        = "gitlab-runner-shell-qemu"
  output         = "${path.root}/../output/gitlab-runner-shell-qemu"
}

source "qemu" "k3s_u22" {
  accelerator      = "kvm"
  cd_files         = ["./cloud-init/*"]
  cd_label         = "cidata"
  disk_compression = true
  disk_image       = true
  disk_size        = "10G"
  headless         = true
  iso_checksum     = "file:https://cloud-images.ubuntu.com/${local.ubuntu_version}/current/SHA256SUMS"
  iso_url          = "https://cloud-images.ubuntu.com/${local.ubuntu_version}/current/${local.ubuntu_version}-server-cloudimg-amd64-disk-kvm.img"
  output_directory = local.output
  shutdown_command = "echo 'packer' | sudo -S shutdown -P now"
  ssh_password     = "ubuntu"
  ssh_username     = "ubuntu"
  vm_name          = "${local.vm_name}.img"
  qemuargs = [
    ["-m", "2048M"],
    ["-smp", "2"],
    ["-serial", "mon:stdio"],
  ]
}

build {
  sources = ["source.qemu.k3s_u22"]

  provisioner "shell" {
    // run scripts with sudo, as the default cloud image user is unprivileged
    execute_command = "echo 'packer' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
    // NOTE: cleanup.sh should always be run last, as this performs post-install cleanup tasks
    scripts = [
      "scripts/install.sh",
      "scripts/cleanup.sh",
    ]
  }

  post-processor "checksum" {
    checksum_types = ["sha1", "sha256", "sha512"]
    output = "${local.output}/${local.vm_name}_{{.ChecksumType}}.checksum"
  }
}

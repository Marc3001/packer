#!/usr/bin/env bash

echo "Cleanup | remove cloud-init uploaded files"
rm -f /home/ubuntu/cc_k3s.py
rm -f /home/ubuntu/cloud.cfg

echo "Cleanup | remove SSH keys used for building"
rm -f /home/ubuntu/.ssh/authorized_keys
rm -f /root/.ssh/authorized_keys

echo "Cleanup | Clear out machine id"
truncate -s 0 /etc/machine-id

echo "Cleanup | Remove the contents of /tmp and /var/tmp"
rm -rf /tmp/* /var/tmp/*

echo "Cleanup | Truncate any logs that have built up during the install"
find /var/log -type f -exec truncate --size=0 {} \;

echo "Cleanup | Cleanup bash history"
rm -f ~/.bash_history

echo "Cleanup | Cleanup apt"
apt-get -y autoremove
sudo apt-get clean
sudo rm -rf /var/lib/apt/lists/*

echo "Cleanup | force a new random seed to be generated"
rm -f /var/lib/systemd/random-seed

export HISTSIZE=0
